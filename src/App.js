import React from 'react';
import Autocomplete from './Components/AutoComplete';

const App = () => {
  return (
    <div className="App">
      <h1>Product Search</h1>
      <Autocomplete apiUrl="https://fakestoreapi.com/products" />
    </div>
  );
};

export default App;





