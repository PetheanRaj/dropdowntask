import React, { useState, useEffect } from 'react';
import axios from 'axios';

const Autocomplete = ({ apiUrl }) => {
    const [inputValue, setInputValue] = useState('');
    const [suggestions, setSuggestions] = useState([]);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
    const [showDropdown, setShowDropdown] = useState(false);

    useEffect(() => {
        const fetchSuggestions = async () => {
            if (inputValue.trim() === '') {
                setSuggestions([]);
                return;
            }

            setLoading(true);
            setError(null);

            try {
                const response = await axios.get(apiUrl);
                const products = response.data;

                // Filter products based on user input
                const filteredSuggestions = products.filter((product) =>
                    product.title.toLowerCase().includes(inputValue.toLowerCase())
                );

                setSuggestions(filteredSuggestions);
            } catch (err) {
                setError('Error fetching suggestions.');
            } finally {
                setLoading(false);
            }
        };

        fetchSuggestions();
    }, [inputValue, apiUrl]);

    const handleInputChange = (e) => {
        setInputValue(e.target.value);
        setShowDropdown(true);
    };

    const handleSuggestionClick = (suggestion) => {
        setInputValue(suggestion.title);
        setShowDropdown(false);
    };

    return (
        <div>
            <input
                type="text"
                value={inputValue}
                onChange={handleInputChange}
                placeholder="Search for products..."
            />

            {loading && <p>Loading...</p>}
            {error && <p>{error}</p>}

            {showDropdown && (
                <ul>
                    {suggestions.map((suggestion) => (
                        <li key={suggestion.id} onClick={() => handleSuggestionClick(suggestion)}>
                            {suggestion.title}
                        </li>
                    ))}
                </ul>
            )}
        </div>
    );
};

export default Autocomplete;
